import { TesteProcessoComponent } from "./teste-processo/teste-processo.component";
import { FormComponent } from "./form/form.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";

const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "form", component: FormComponent },
  { path: "empresa", component: TesteProcessoComponent }
  // { path: "empresa", component: TesteProcessoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
