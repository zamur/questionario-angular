import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

export interface Step {
  categoria: string;
  p1: string;
  p2: string;
  p3: string;
  p4: string;
}

const ITEM1: Step[] = [
  {
    categoria: "Conscientização",
    p1:
      "A diretoria reconhece a necessidade de melhorar a gestão de processos para melhorar o desempenho operacional, mas tem compreensão limitada de como fazer isso.",
    p2:
      "Pelo menos um executivo sênior entende com profundidade o conceito de processos de negócio e como a empresa pode usá-lo para melhorar seu desempenho operacional.",
    p3:
      "A diretoria não percebe a empresa sob uma estrutura funcional tradicional, mas sim orientada por processos.",
    p4:
      "A diretoria encara seu próprio trabalho na ótica de processos e percebe a gestão de processos não como um projeto, mas sim como uma forma de gerir o negócio."
  },
  {
    categoria: "Alinhamento",
    p1:
      "Existe um programa na empresa para gestão orientada por processos e o líder desse programa é um gerente.",
    p2:
      "O líder e responsável pelo programa de processos é um superintendente ou um diretor da empresa.",
    p3:
      "A diretoria está alinhada com o programa de processos. Há pessoas chaves na empresa para ajudar a promover iniciativas de processos.",
    p4:
      "Toda a empresa está entusiasmada com a gestão orientada por processos e auxilia as iniciativas relacionadas a processos."
  },
  {
    categoria: "Conduta",
    p1:
      "Existe um superintendente ou um diretor que apoia e investe em desenvolvimento operacional.",
    p2:
      "Existe um superintendente ou um diretor que define metas de desempenho ousadas e para alcançá-las está preparado para alocar recursos, fazer profundas mudanças e remover obstáculos.",
    p3:
      "Superintendentes e diretores atuam como equipe, conduzem a empresa com base em seus processos e estão ativamente envolvidos no programa de processos.",
    p4:
      "O trabalho dos superintendentes e diretores é orientado por processos, o planejamento estratégico é focado em processos e novas oportunidades de negócios são analisadas com base no desempenho de seus processos."
  },
  {
    categoria: "Estilo",
    p1:
      "Os superintendentes e diretores começaram a mudar de um estilo autoritário e hierárquico para um estilo aberto e colaborativo.",
    p2:
      "Os superintendentes e diretores que lideram o programa de processos estão convencidos da necessidade de mudança e enxergam a gestão de processos como uma ferramenta chave.",
    p3:
      "Os superintendentes e diretores delegam o controle e a autoridade aos responsáveis pelos processos e aos executores dos processos.",
    p4:
      "Os superintendentes e diretores exercem liderança por meio de visão e influência, e não por meio de comando e controle."
  }
];

const ITEM2: Step[] = [
  {
    categoria: "Trabalho em equipe",
    p1:
      "O trabalho em equipe é foco apenas durante projetos específicos e iniciativas atípicas.",
    p2:
      "A empresa geralmente forma equipes multifuncionais para iniciativas de melhoria.",
    p3:
      "O trabalho em equipe é comum entre quem executa os processos e entre os gestores.",
    p4:
      "O trabalho em equipe com clientes e fornecedores é prática corriqueira da empresa."
  },
  {
    categoria: "Foco no cliente",
    p1:
      "Há uma crença generalizada de que o foco no cliente é importante, mas há também incertezas e conflitos sobre como atender às necessidades dos clientes.",
    p2:
      "Os colaboradores entendem que o objetivo do trabalho é entregar valor ao cliente.",
    p3:
      "Os colaboradores compreendem que os clientes exigem um padrão de excelência no produto vendido ou serviço prestado.",
    p4:
      "Os colaboradores enxergam a necessidade de buscar parcerias comerciais para atender as necessidades dos clientes finais."
  },
  {
    categoria: "Responsabilidade",
    p1: "A responsabilidade pelos resultados recai somente sobre os gestores.",
    p2:
      "Além dos gestores, a equipe de operação começa a assumir também responsabilidades pelos resultados.",
    p3:
      "Todos os colaboradores se sentem responsáveis pelos resultados da empresa.",
    p4:
      "Todos os colaboradores se sentem responsáveis em atender os clientes e alcançar um desempenho cada vez melhor."
  },
  {
    categoria: "Postura em relação à mudança",
    p1:
      "A empresa entende que há a necessidade de realizar algumas pequenas mudanças.",
    p2:
      "O colaboradores estão preparados para mudanças significativas na forma como o trabalho é realizado.",
    p3:
      "Os colaboradores estão prontos para a grande e importante mudança multidimensional.",
    p4:
      "Os colaboradores reconhecem a mudança como inevitável e a acolhem sem resistência."
  }
];

const ITEM3: Step[] = [
  {
    categoria: "Pessoal",
    p1:
      "A empresa possui um pequeno grupo de pessoas altamente cientes da importante de gerir os processos.",
    p2:
      "A empresa possui um grupo de especialistas com habilidades em redesenho de processos, gestão de projetos e gestão de mudanças.",
    p3:
      "A empresa possui um grupo de especialistas capacitados em redesenho de processos, gestão de projetos e gestão de mudanças em grande escala.",
    p4:
      "Há número representativo de pessoas capacitadas em gestão de processos, projetos, programas e mudanças. Há processo formal para desenvolver e manter esse quadro."
  },
  {
    categoria: "Metodologia",
    p1:
      "A empresa usa uma ou mais metodologias para resolver problemas de execução e promover avanços incrementais nos processos.",
    p2:
      "Existe uma equipe de reengenharia de processos e ela segue uma metodologia básica.",
    p3:
      "A empresa tem um processo formal e padronizado de reengenharia de processos.",
    p4:
      "Reengenharia de processos é uma competência básica e integra um sistema formal que inclui monitoramento do ambiente, planejamento de mudanças, implementação e inovação focados em processos."
  }
];

const ITEM4: Step[] = [
  {
    categoria: "Modelo de Processo",
    p1:
      "A empresa reconhece e identifica alguns processos de negócios (processos que entregam valor para o cliente ou apoiam outros processos).",
    p2:
      "A empresa desenvolveu um modelo completo de processos organizacionais e uma equipe sênior (gerentes ou superintendentes) já autorizou e consentiu.",
    p3:
      "O modelo de processos da empresa foi disseminado por toda a organização, sendo utilizado para definir prioridades e vinculado a tecnologias.",
    p4:
      "O modelo de processos da empresa se conecta com o modelo de clientes e fornecedores. Ele também é usado na definição do planejamento estratégico."
  },
  {
    categoria: "Responsabilidade",
    p1:
      "Gerentes funcionais são responsáveis pelo desempenho de suas área específicas e gerentes de projetos por iniciativas de aprimoramento.",
    p2:
      "Existe um comitê de direção que é responsável pelo progresso global dos processos da empresa.",
    p3:
      "Os proprietários de um processo específico dividem a responsabilidade pelo desempenho global dos processos da empresa.",
    p4:
      "Um conselho de processos atua como a instância mais elevada da gestão. Executores e proprietários dividem a responsabilidade pelos processos visando o desempenho da empresa."
  },
  {
    categoria: "Integração",
    p1:
      "Um ou mais grupos defendem e apoiam técnicas de aprimoramento de processos possivelmente distintas.",
    p2:
      "Existe um grupo informal para gerir o programa de processos e um comitê supervisor que aloca recursos para projetos de reengenharia de processos.",
    p3:
      "Há uma estrutura formal centralizada, coordenada por um diretor, para gerir o programa de processos. Além disso, existe um conselho de processos que cuida das questões relacionadas à integração inter-processos.",
    p4:
      "Os proprietários dos processos trabalham com colegas do mesmo nível focando nos clientes e fornecedores, de forma a promover a integração dos processos inter-empresas."
  }
];

@Component({
  selector: "app-teste-processo",
  templateUrl: "./teste-processo.component.html",
  styleUrls: ["./teste-processo.component.scss"]
})
export class TesteProcessoComponent implements OnInit {
  opts = ["Totalmente verdadeira", "Parcialmente verdadeira", "Não verdadeira"];
  displayedColumns: string[] = ["categoria", "p1", "p2", "p3", "p4"];
  dataSource1 = ITEM1;
  dataSource2 = ITEM2;
  dataSource3 = ITEM3;
  dataSource4 = ITEM4;

  isLinear = false;
  formGroupLideranca: FormGroup;
  formGroupCultura: FormGroup;
  formGroupConhecimento: FormGroup;
  formGroupGovernanca: FormGroup;

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.formGroupLideranca = this._formBuilder.group({
      p1: ["", Validators.required],
      p2: ["", Validators.required],
      p3: ["", Validators.required],
      p4: ["", Validators.required],
      p5: ["", Validators.required],
      p6: ["", Validators.required],
      p7: ["", Validators.required],
      p8: ["", Validators.required],
      p9: ["", Validators.required],
      p10: ["", Validators.required],
      p11: ["", Validators.required],
      p12: ["", Validators.required],
      p13: ["", Validators.required],
      p14: ["", Validators.required],
      p15: ["", Validators.required],
      p16: ["", Validators.required]
    });

    this.formGroupCultura = this._formBuilder.group({
      p1: ["", Validators.required],
      p2: ["", Validators.required],
      p3: ["", Validators.required],
      p4: ["", Validators.required],
      p5: ["", Validators.required],
      p6: ["", Validators.required],
      p7: ["", Validators.required],
      p8: ["", Validators.required],
      p9: ["", Validators.required],
      p10: ["", Validators.required],
      p11: ["", Validators.required],
      p12: ["", Validators.required],
      p13: ["", Validators.required],
      p14: ["", Validators.required],
      p15: ["", Validators.required],
      p16: ["", Validators.required]
    });

    this.formGroupConhecimento = this._formBuilder.group({
      p1: ["", Validators.required],
      p2: ["", Validators.required],
      p3: ["", Validators.required],
      p4: ["", Validators.required],
      p5: ["", Validators.required],
      p6: ["", Validators.required],
      p7: ["", Validators.required],
      p8: ["", Validators.required]
    });

    this.formGroupGovernanca = this._formBuilder.group({
      p1: ["", Validators.required],
      p2: ["", Validators.required],
      p3: ["", Validators.required],
      p4: ["", Validators.required],
      p5: ["", Validators.required],
      p6: ["", Validators.required],
      p7: ["", Validators.required],
      p8: ["", Validators.required],
      p9: ["", Validators.required],
      p10: ["", Validators.required],
      p11: ["", Validators.required],
      p12: ["", Validators.required]
    });
  }

  enviar() {
    console.log(this.formGroupGovernanca);
  }
}
