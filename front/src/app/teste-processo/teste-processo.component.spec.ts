import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TesteProcessoComponent } from './teste-processo.component';

describe('TesteProcessoComponent', () => {
  let component: TesteProcessoComponent;
  let fixture: ComponentFixture<TesteProcessoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TesteProcessoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TesteProcessoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
